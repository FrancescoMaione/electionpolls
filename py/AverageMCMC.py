import numpy as np
import scipy
from scipy import stats
import pymc
from matplotlib import pyplot as plt
from pymc.Matplot import plot as mcplot

exec(open("/home/toroseduto/Documents/Possibile/ReferendumCostituzionale/Sondaggi/py/Average538.py").read())
si = np.zeros(len(d))
t = np.zeros(len(d))
std = np.zeros(len(d))
inst = []
zero = dt.strptime('12/04/2016','%d/%m/%Y')
tmax = (election- zero).days
for i,ss in enumerate(d):
    si[i] = d[i]['si']*0.01
    t[i] = int(tmax-d[i]['t'])
    std[i] = d[i]['std_error']
    inst.append(d[i]['institute'])
#tmax_reduced = tmax-(election-dt.today()).days
tmax_reduced = tmax
instU, inst_index = np.unique(inst,return_inverse=True)
#omega = pymc.Uniform("omega",0,0.01) #Small, as expected. This is an informed prior!
omega = pymc.HalfCauchy("omega",0,0.01) #Suggested by...
#Try also inverse prior for omega!
#omega2 = pymc.Pareto("omega2",1,1)
#Sdelta = pymc.("Sdelta",0,0.05)
Mdelta = pymc.Normal("Mdelta",0,1.0/0.02**2) 
delta = pymc.Normal("delta",[Mdelta for i in range(len(instU))],1.0/(0.075**2)*np.ones(len(instU)),size=len(instU)) #Sdelta = 0.075, its big in order to let the model choose. 
alpha_0 = pymc.Uniform("alpha_0",0.4,0.6)
alpha = np.empty(tmax_reduced,dtype=object)
alpha[0]=alpha_0
for i in range(1,tmax_reduced):
    alpha[i] = pymc.Normal('alpha_%d'% i, mu=alpha[i-1], tau = 1.0/omega**2)
y = pymc.Normal("y",np.array([alpha[i] for i in t]),1.0/(std**2),value=si,size=len(si),observed=True)
mcmc = pymc.MCMC([y,alpha,delta,Mdelta,omega])
mcmc.sample(100000,5000,1)
alpha_trace = np.empty(tmax_reduced,dtype=object)
alpha_mean = np.zeros(tmax_reduced)
alpha_std = np.zeros(tmax_reduced)
for i in range(tmax_reduced):
    alpha_trace[i] = mcmc.trace("alpha_%d" %i)[:]
    alpha_std[i] = alpha_trace[i].std()
    alpha_mean[i] = alpha_trace[i].mean()

om = mcmc.trace("omega")[:]
print("Media omega",om.mean())
alpha_ecdf = ECDF(alpha_trace[-1])
if (alpha_mean[-1] > 0.5):
    pr_si = 1-alpha_ecdf(0.5)
    #pr_si = 0.5 + 0.5*pr_si
    print("Probabilità vittoria del sì è %2.2e" %pr_si)
    ind=indecisi/100/(alpha_mean[-1]-0.5)/2
    ind = max(ind,0.1)
    pr_si_adj = max(0.5,pr_si-ind)
    print("prob sì corretta con indecisi è %1.3e" %pr_si_adj)
else:
    pr_no = alpha_ecdf(0.5)
#    pr_no = 0.5 + 0.5*pr_no
    print("Probabilità vittoria del no è %2.5e" %pr_no)
    ind=indecisi/100/(0.5-alpha_mean[-1])/2
    ind = min(ind,0.1)
    pr_no_adj = max(0.5,pr_no-ind)
    print("prob no corretta con indecisi è %1.3e" %pr_no_adj)

time = range(tmax_reduced)
maggio = dt.strptime('01/05/2016','%d/%m/%Y')
giugno = dt.strptime('01/06/2016','%d/%m/%Y')
luglio = dt.strptime('01/07/2016','%d/%m/%Y')
agosto = dt.strptime('01/08/2016','%d/%m/%Y')
settembre = dt.strptime('01/09/2016','%d/%m/%Y')
ottobre = dt.strptime('01/10/2016','%d/%m/%Y')
novembre = dt.strptime('01/11/2016','%d/%m/%Y')

maggio_d = tmax-(election-maggio).days
giugno_d = tmax-(election-giugno).days
luglio_d = tmax-(election-luglio).days
agosto_d = tmax-(election-agosto).days
settembre_d = tmax-(election-settembre).days
ottobre_d = tmax-(election-ottobre).days
novembre_d = tmax-(election-novembre).days

plt.figure()
plt.plot(time,alpha_mean)
plt.fill_between(time,alpha_mean-1.96*alpha_std,alpha_mean+1.96*alpha_std,facecolor='yellow',alpha=0.5)
plt.fill_between(time,alpha_mean-alpha_std,alpha_mean+alpha_std,facecolor='green',alpha=0.5)
plt.plot(t,si,'or')
plt.axhline(0.5,c='k')
plt.axvline(maggio_d,c='r')
plt.axvline(giugno_d,c='r')
plt.axvline(luglio_d,c='r')
plt.axvline(agosto_d,c='r')
plt.axvline(settembre_d,c='r')
plt.axvline(ottobre_d,c='r')
plt.axvline(novembre_d,c='r')
#plt.xlim(0,ottobre_d+30)
plt.ylim(min(si)-0.05,max(si)+0.05)
plt.xlabel("t")
plt.ylabel("Frazione sostenitori sì")
#plt.axvline(ottobre_d,c='r')
#plt.axvline(novembre_d,c='r')
plt.savefig("./Costituzionale_Bayes.png")
#plt.figure()
#omega_trace=mcmc.trace("omega")
#plt.hist(omega_trace)
#plt.savefig("./omega.png")




#plt.figure()
#mcplot(mcmc.trace("Sdelta"))
#mcplot(mcmc.trace("Mdelta"))
#mcplot(mcmc.trace("alpha_60"))
mcplot(mcmc.trace("omega"))
mcplot(mcmc.trace("delta"))
#mcplot(mcmc.trace("delta"))
plt.show()
