import numpy as np
from datetime import datetime as dt
import pymc
from pymc.Matplot import plot as mcplot
from statsmodels.distributions.empirical_distribution import ECDF

election=dt.strptime('04/12/2016','%d/%m/%Y')
s = np.loadtxt('/home/toroseduto/Documents/Possibile/ReferendumCostituzionale/Sondaggi/data/SondaggiCostituzionale',dtype='S20')
s = s.astype(str)
ro = np.loadtxt('/home/toroseduto/Documents/Possibile/ReferendumCostituzionale/Sondaggi/data/SondaggiRoma',dtype='S20')
ro = ro.astype(str)
na = np.loadtxt('/home/toroseduto/Documents/Possibile/ReferendumCostituzionale/Sondaggi/data/SondaggiNapoli',dtype='S20')
na = na.astype(str)
to = np.loadtxt('/home/toroseduto/Documents/Possibile/ReferendumCostituzionale/Sondaggi/data/SondaggiTorino',dtype='S20')
to = to.astype(str)
mi = np.loadtxt('/home/toroseduto/Documents/Possibile/ReferendumCostituzionale/Sondaggi/data/SondaggiMilano',dtype='S20')
mi = mi.astype(str)
eu = np.loadtxt('/home/toroseduto/Documents/Possibile/ReferendumCostituzionale/Sondaggi/data/SondaggiEU2014PD',dtype='S20')
eu = eu.astype(str)
d = np.empty(len(s),dtype=object)
hist = np.ones(5)
res_hist = np.array([24.91,21.13,41.83,41.69,40.1])
si = 0.0
no = 0.0
voto= 0.0
indecisi = 0.0
wtot = 0.0
wtot_voto = 0.0
wtot_ind = 0.0
err2=0.0
for i,ss in enumerate(s):
    d[i]=dict()
    d[i]['institute']=s[i][0]
    d[i]['date_start']=dt.strptime(s[i][1],'%d/%m/%Y')
    d[i]['date_end']=dt.strptime(s[i][2],'%d/%m/%Y')
    d[i]['method']=s[i][3]
    d[i]['N']=int(s[i][4])
    d[i]['si']=float(s[i][5])
    d[i]['no']=float(s[i][6])
    d[i]['voto']=float(s[i][7])
    d[i]['non_voto']=float(s[i][8])
    d[i]['non_so']=float(s[i][9])
    d[i]['FRsino']=float(s[i][10])
    d[i]['date']=dt.fromordinal(int(np.ceil((d[i]['date_start'].toordinal()+d[i]['date_end'].toordinal())/2)))
    d[i]['t']=election-d[i]['date']
    d[i]['t']=d[i]['t'].days
    d[i]['t0']=(dt.now()-d[i]['date']).days
    d[i]['w']=np.sqrt(d[i]['N'])*0.5**(d[i]['t0']/(14+0.2*d[i]['t']))
    w_affl = np.sqrt(d[i]['N'])*0.5**(d[i]['t0']/(14+0.1*d[i]['t']))
    d[i]['std_error']=np.sqrt(d[i]['si']*0.01*d[i]['no']*0.01/d[i]['N'])
    err2 = err2 + d[i]['std_error']**2*d[i]['w']**2
    si = si + d[i]['si']*d[i]['w']
    no = no + d[i]['no']*d[i]['w']
    if (d[i]['voto']-50 > 1e-2):
        voto = voto + d[i]['voto']*d[i]['w']
        wtot_voto = wtot_voto+d[i]['w']
    indecisi = indecisi + (1-d[i]['FRsino'])*w_affl
    wtot_ind = wtot_ind+w_affl
    wtot = wtot + d[i]['w']
for i,ss in enumerate(s):
    print(d[i]['institute'],d[i]['N'],d[i]['t'],d[i]['w']/wtot,d[i]['si']+d[i]['no'])
err_st = np.sqrt(err2)/wtot*100
si = si/wtot
no = no/wtot
voto = voto/wtot_voto
indecisi = indecisi/wtot_ind

com = [ro,na,to,mi]
comunali=np.zeros(5)
idx_com = [5,5,4,4]



election_com = dt.strptime('05/05/2016','%d/%m/%Y')
#now_com = election_com-election-dt.now()
for c,res in enumerate(com):
    pd = 0
    wtot = 0
    for i,rr in enumerate(res):
        N = int(res[i][3])
        t0=dt.strptime(res[i][1],'%d/%m/%Y')
        t1=dt.strptime(res[i][2],'%d/%m/%Y')
        date=dt.fromordinal(int(np.ceil((t0.toordinal()+t1.toordinal())/2)))
        ti=election_com-date
        ti=ti.days
        T=(election_com-date).days
        w = np.sqrt(N)*0.5**(T/(14+0.2*ti))
        pd = pd + float(res[i][idx_com[c]])*w
        wtot = wtot + w
    comunali[c] = pd/wtot
#print("done com")

election_eu=dt.strptime('25/04/2014','%d/%m/%Y')
pd = 0
wtot = 0
for j,ee in enumerate(eu):
    N = int(eu[j][3])
    t0=dt.strptime(eu[j][1],'%d/%m/%Y')
    t1=dt.strptime(eu[j][2],'%d/%m/%Y')
    date=dt.fromordinal(int(np.ceil((t0.toordinal()+t1.toordinal())/2)))
    ti=election_eu-date
    ti=ti.days
    T=(election_eu-date).days
    w = np.sqrt(N)*0.5**(T/(14+0.2*ti))
    pd = pd + float(eu[j][4])*w
    wtot = wtot + w
comunali[4] = pd/wtot


err = abs(comunali-res_hist)
av_err = pymc.Uniform("av",0,10)
std_err = pymc.HalfCauchy("std",0,5)
err_l = pymc.NoncentralT("err_l",av_err,std_err,10,value=err,observed=True)
mcerr = pymc.MCMC([err_l,av_err,std_err])
mcerr.sample(50000)
av_trace = mcerr.trace("av")[:]
av_mean = av_trace.mean()
av_std = av_trace.std()

if (si > no):
    ecdf = ECDF(av_trace)
    pr_si = ecdf(si-50.0)
    pr_si = 50 + 0.5*pr_si*100
    print("Probabilità vittoria del sì è %2.2e" %pr_si)
else:
    ecdf = ECDF(av_trace)
    pr_no = ecdf(no-50.0)
    print("pr ecdf",0.5*pr_no)
    pr_no = 50 + 0.5*pr_no*100
    print("Probabilità vittoria del no è %2.5e" %pr_no)

print('sì = ',si,'%')
print('no = ',no,'%')
print('voto = ',voto,'%')
print('indecisi = ',indecisi*100,'%')
print('err = ',err_st,'%')

print('historical error:',err)
print('average historical error',av_mean)
print('average historical error std',av_std)
mcplot(mcerr.trace("av"))
