import numpy as np
import scipy
from scipy import stats
import pymc
from matplotlib import pyplot as plt
from pymc.Matplot import plot as mcplot

exec(open("/home/toroseduto/Documents/Possibile/ReferendumCostituzionale/Sondaggi/py/Average538.py").read())
si = np.zeros(len(d))
t = np.zeros(len(d))
std = np.zeros(len(d))
inst = []
tmax = 172
for i,ss in enumerate(d):
    si[i] = d[i]['si']*0.01
    t[i] = int(tmax-d[i]['t'])
    std[i] = d[i]['std_error']
    inst.append(d[i]['institute'])
tmax_reduced = tmax-(election-dt.today()).days
instU, inst_index = np.unique(inst,return_inverse=True)
omega = pymc.Uniform("omega",1e-3,0.02)
#Sdelta = pymc.Uniform("sdelta",1e-5,0.01)
#Mdelta = pymc.Normal("Mdelta",0,1.0/0.006**2)
#delta = pymc.Normal("delta",[Mdelta for i in range(len(instU))],[1.0/Sdelta**2 for i in range(len(instU))],size=len(instU))
#alpha = np.empty(tmax_reduced,dtype=object)
#alpha[0]=pymc.Uniform("alpha0",0.4,0.6)
#for i in range(tmax_reduced-1):
#    alpha[i+1]=pymc.Normal("alpha"+str(i+1),alpha[i],1.0/(omega**2))
alpha_0 = pymc.Uniform("alpha_0",0.4,0.6)
alpha = np.empty(tmax_reduced,dtype=object)
alpha[0]=alpha_0
for i in range(1,tmax_reduced):
    alpha[i] = pymc.Normal('alpha_%d'% i, mu=alpha[i-1], tau = 1.0/omega**2)
y = pymc.Normal("y",np.array([alpha[i] for i in t]),1.0/(std**2),value=si,size=len(si),observed=True)
#alpha = pymc.Container(alpha)
#model = pymc.Model([omega,alpha,y])
#map_ = pymc.MAP(model)
#map_.fit()
mcmc = pymc.MCMC([y,alpha,omega])
mcmc.sample(500000,50000,1)
alpha_trace = np.empty(tmax_reduced,dtype=object)
alpha_mean = np.zeros(tmax_reduced)
alpha_std = np.zeros(tmax_reduced)
for i in range(tmax_reduced):
    alpha_trace[i] = mcmc.trace("alpha_%d" %i)[:]
    alpha_std[i] = alpha_trace[i].std()
    alpha_mean[i] = alpha_trace[i].mean()

time = range(tmax_reduced)
plt.figure()
plt.plot(time,alpha_mean)
plt.fill_between(time,alpha_mean-1.96*alpha_std,alpha_mean+1.96*alpha_std,facecolor='yellow',alpha=0.5)
plt.fill_between(time,alpha_mean-alpha_std,alpha_mean+alpha_std,facecolor='green',alpha=0.5)
plt.plot(t,si,'or')
plt.axhline(0.5,c='k')
plt.savefig("./Costituzionale_Bayes_nohouse.png")
plt.figure()
mcplot(mcmc.trace("omega"))
mcplot(mcmc.trace("alpha_56"))
plt.show()
